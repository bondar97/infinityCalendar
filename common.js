		var allMonses = [
		"",
		"Январь",
		"Февраль",
		"Март",
		"Апрель",
		"Май",
		"Июнь",
		"Июль",
		"Август",
		"Сентябрь",
		"Октябрь",
		"Ноябрь",
		"Декабрь"
		];

		Date.prototype.daysInMonth = function() {
			return 32 - new Date(this.getFullYear(), this.getMonth(), 32).getDate();
		};

    var thisDayButton = document.getElementById('thisDayButton');
    var mainContent = document.getElementById('main');
    var clickPrevious = document.getElementById('previous');
    var clickNext = document.getElementById('next');

		var today = new Date();
		var thisDay = today.getFullYear();
		var thisMonth = today.getMonth()+1;
		var ThisDayInThisMonyhAndYear = today.getDate();

    //next
    var todayNext = new Date();
		var thisDayNext = todayNext.getFullYear();
		var thisMonthNext = todayNext.getMonth()+1;

		//previous
    var todayPrevious = new Date();
		var thisDayPrevious = todayPrevious.getFullYear();
		var thisMonthPrevious = todayPrevious.getMonth()+1;

		function addOtherEvents() {
			for (var r = 0; r < myObjects.length; r++) {
				var year2 = document.getElementById('nextYear');
				var year2Number = year2.innerHTML.split(" ");
				var month2 = document.getElementById('nextMonth');
				var monthNumber2 = month2.innerHTML.split(" ");
				var monthNumber3 = monthNumber2[0];
				var monthResult2 = 0;
				for (var f = 0; f < allMonses.length; f++) {
					if (monthNumber3===allMonses[f]) {
						monthResult2 = f;
					}            
				}   
				if (myObjects[r][3]==year2Number[0] && myObjects[r][4]==monthResult2) {
					console.log(year2Number[0])
					for (var t = 0; t < mainContent.children.length; t++) {
						if (mainContent.children[t].innerHTML==myObjects[r][5]) {
							console.log(mainContent.children[t]);
							console.log(myObjects[r]); 
							mainContent.children[t].style.backgroundColor="#d7eef3";
							var wrapper = document.createElement("div");  
							wrapper.className = "myEvent";
							mainContent.children[t].appendChild(wrapper);
							var myForm = document.getElementById('myForm');
							var myForm3 = myForm.cloneNode(true);
							wrapper.appendChild(myForm3);
							var insidValue = myForm3.getElementsByTagName('input');
							insidValue[0].value = myObjects[r][0];
							insidValue[1].value = myObjects[r][1];
							insidValue[2].value = myObjects[r][2];  
							myForm3.style.display = "none"; 
						}
					}
				}
			}	
		}
    
		function navigationYearMonth(month, year) {
			this.year = year;
			this.month = month;
			var control = document.getElementById('control');
			var newMonth = document.createElement("span");
			newMonth.id = "nextMonth";
			newMonth.innerHTML = this.month + " ";
			control.appendChild(newMonth);
			var newYear = document.createElement("span");
			newYear.id = "nextYear";
			newYear.innerHTML = this.year+" год ";
			control.appendChild(newYear);
		}    

		function thisMonthThisYear() {
			var today1 = new Date();
			var thisDay1 = today1.getFullYear();
			var thisMonth1 = today1.getMonth()+1;
			while (mainContent.firstChild) {
				mainContent.removeChild(mainContent.firstChild);
			}
			var todayDate = new navigationYearMonth(allMonses[thisMonth], thisDay)
			var nextMonth = document.getElementById('nextMonth');
			control.removeChild(nextMonth); 
			var nextYear = document.getElementById('nextYear');
			control.removeChild(nextYear);   
			todayDate = new navigationYearMonth(allMonses[thisMonth], thisDay)     
			var date1 = new Date(thisDay1, (thisMonth1-1));
			var dayNumber1 = date1.daysInMonth();
			var todayDayinThisMonth = today1.getDate();    

			for (var i = 1; i < date1.getDay(); i++) {
				var newDaySpan1 = document.createElement("div");
				newDaySpan1.innerHTML = "_";
				newDaySpan1.className = "someDay";
				mainContent.appendChild(newDaySpan1); 
			}

			for (var i = 1; i <= dayNumber1; i++) {     	
				var newDaySpan = document.createElement("div");
				newDaySpan.innerHTML = i;
				newDaySpan.className = "someDay";
				mainContent.appendChild(newDaySpan); 
				if (i==todayDayinThisMonth) {
					mainContent.removeChild(mainContent.lastChild);
					var newDaySpan = document.createElement("div");
					newDaySpan.innerHTML = i;
					newDaySpan.className = "myday";
					mainContent.appendChild(newDaySpan); 
				}

				else if(myObjects!==""&&myObjects!==undefined){
					console.log("myObjects");
					addOtherEvents() 
				}
			}
			thisDayNext = todayNext.getFullYear();
			thisMonthNext = todayNext.getMonth()+1;
			thisDayPrevious = todayPrevious.getFullYear();
			thisMonthPrevious = todayPrevious.getMonth()+1;
		};

		function thisMonthThisYear1() {
			var nextMonth = document.getElementById('nextMonth');
			control.removeChild(nextMonth); 
			var nextYear = document.getElementById('nextYear');
			control.removeChild(nextYear); 
			thisMonthThisYear();
		}
     thisDayButton.addEventListener('click', thisMonthThisYear1);
     //вызов по дефолту при загрузке
     thisMonthThisYear()

//next    
    function  createDelete() {
    	var control = document.getElementById('control');
    	var nextMonth = document.getElementById('nextMonth');
    	control.removeChild(nextMonth); 
    	var nextYear = document.getElementById('nextYear');
    	control.removeChild(nextYear);      	
    }

    function  changeFunction() {
    	var date = new Date(thisDayNext, (thisMonthNext-1));
    	var dayNumber = date.daysInMonth();
    	while (mainContent.firstChild) {
    		mainContent.removeChild(mainContent.firstChild);
    	}
    	if (date.getDay()==0) {
    		for (var i = 1; i < 7; i++) {
    			var newDaySpan = document.createElement("div");
    			newDaySpan.innerHTML = "_";
    			newDaySpan.className = "someDay";
    			mainContent.appendChild(newDaySpan);
    		}
    	}
    	for (var i = 1; i < date.getDay(); i++) {
    		var newDaySpan = document.createElement("div");
    		newDaySpan.innerHTML = "_";
    		newDaySpan.className = "someDay";
    		mainContent.appendChild(newDaySpan);
    	}
    	var year3 = document.getElementById('nextYear');
    	var year3Number = year3.innerHTML.split(" ");
    	var month3 = document.getElementById('nextMonth');
    	var monthNumber3 = month3.innerHTML.split(" ");
    	var monthNumber4 = monthNumber3[0];
    	var monthResult3 = 0;
    	for (var f = 0; f < allMonses.length; f++) {
    		if (monthNumber4===allMonses[f]) {
    			monthResult3 = f;
    		}            
    	}
    	for (var i = 1; i < dayNumber+1; i++) {
    		if (i==ThisDayInThisMonyhAndYear && year3Number[0]==thisDay && monthResult3==thisMonth) {
    			var newDaySpan1 = document.createElement("div");
    			newDaySpan1.innerHTML = i;
    			newDaySpan1.className = "myday";
    			mainContent.appendChild(newDaySpan1); 
    			i++
    		}
    		var newDaySpan = document.createElement("div");
    		newDaySpan.innerHTML = i;
    		newDaySpan.className = "someDay";
    		mainContent.appendChild(newDaySpan);         
    		addOtherEvents()
    	} 
    }

    clickNext.addEventListener('click', function() {   
    	if (thisMonthNext==12) {
    		thisMonthNext=1;
    		thisDayNext++
    		createDelete();
    		var todayDateMonth = new navigationYearMonth(allMonses[thisMonthNext], thisDayNext)
    		changeFunction()
    	}    	
    	else{
    		thisMonthNext++
    		createDelete();
    		var todayDateMonth = new navigationYearMonth(allMonses[thisMonthNext], thisDayNext)
    		changeFunction()    		
    	}
    });

    //previous
    function changeFunctionForPrevious(){
    	var date = new Date(thisDayNext, (thisMonthNext-1));
    	var dayNumber = date.daysInMonth();
    	while (mainContent.firstChild) {
    		mainContent.removeChild(mainContent.firstChild);
    	}
    	console.log("date.getDay() = "+date.getDay())
    	if (date.getDay()==0) {
    		for (var i = 1; i < 7; i++) {
    			var newDaySpan = document.createElement("div");
    			newDaySpan.innerHTML = "_";
    			newDaySpan.className = "someDay";
    			mainContent.appendChild(newDaySpan);
    		}
    	}
    	for (var i = 1; i < date.getDay(); i++) {
    		var newDaySpan = document.createElement("div");
    		newDaySpan.innerHTML = "_";
    		newDaySpan.className = "someDay";
    		mainContent.appendChild(newDaySpan);
    	}
    	var year4 = document.getElementById('nextYear');
    	var year4Number = year4.innerHTML.split(" ");
    	var month4 = document.getElementById('nextMonth');
    	var monthNumber4 = month4.innerHTML.split(" ");
    	var monthNumber5 = monthNumber4[0];
    	var monthResult4 = 0;
    	for (var n = 0; n < allMonses.length; n++) {
    		if (monthNumber5===allMonses[n]) {
    			monthResult4 = n;
    		}            
    	}
    	for (var i = 1; i < dayNumber+1; i++) {
    		if (i==ThisDayInThisMonyhAndYear && year4Number[0]==thisDay && monthResult4==thisMonth) {
    			var newDaySpan1 = document.createElement("div");
    			newDaySpan1.innerHTML = i;
    			newDaySpan1.className = "myday";
    			mainContent.appendChild(newDaySpan1); 
    			i++
    		}
    		var newDaySpan = document.createElement("div");
    		newDaySpan.innerHTML = i;
    		newDaySpan.className = "someDay";
    		mainContent.appendChild(newDaySpan);
    		addOtherEvents()
    	}
    }

    clickPrevious.addEventListener('click', function() {
    	if (thisMonthNext==1) {
    		thisMonthNext=12;
    		thisDayNext--
    		createDelete();   
    		var todayDateMonth = new navigationYearMonth(allMonses[thisMonthNext], thisDayNext)
    		changeFunctionForPrevious()
    	}    	
    	else{
    		thisMonthNext--
    		createDelete();   
    		var todayDateMonth = new navigationYearMonth(allMonses[thisMonthNext], thisDayNext)
    		changeFunctionForPrevious()
    	}
    });

    var counterEvent = 1;
    var myForm2 ="-";

    mainContent.addEventListener('click', function(event) {
    	var target = event.target; 
    	var flag = 0;
    	var getCurentBlock1 = target;
    	if (flag==0&&target.innerHTML=="_") {
    		alert("Здесь пусто! пробуй еще!")
    	}

    	else if(flag==0&&target.children[0]!==undefined){
    		var getChild = target.children[0].children[0];
    		getChild.style.display = "block";
    	}

    	else if(flag==0&&target.className == "someDay"&&target.children[0]==undefined){   
    		var wrapper = document.createElement("div");    		
    		wrapper.className = "myEvent";
    		target.appendChild(wrapper);
    		var myForm = document.getElementById('myForm');
    		myForm2 = myForm.cloneNode(true);
    		myForm2.getElementsByTagName('input')[0].value = '';
    		myForm2.getElementsByTagName('input')[1].value = '';
    		myForm2.getElementsByTagName('input')[2].value = '';
    		myForm2.style.display = "block";
    		wrapper.appendChild(myForm2);       
    	}

    	else if(flag==0&&target.id == "clothe"){
    		var getParent = target.parentElement;
    		getParent.style.display = "none";
    	}

    	else if(flag==0&&target.id == "create"){
    		var getParent = target.parentElement;
    		var getCurentBlock = target.parentElement.parentElement.parentElement;
    		var inputes = myForm2.children;       
    		if(inputes[0].value==""){
    			alert("Введите название ивента");
    		}
    		else if (inputes[0].value!=="") {
    			var num = 1;
    			for (var i = 0; i < myObjects.length; i++) {
    				if(myObjects[i][0]===inputes[0].value){
    					alert("Уже есть такой ивент!");
    					inputes[0].value = "";
    					getCurentBlock.style.backgroundColor="#fff";
    					num=2;
    				}
    			}
    			if (num!==2) {
    				getCurentBlock.style.backgroundColor="#d7eef3";
    				var year1 = document.getElementById('nextYear');
    				var year1Number = year1.innerHTML.split(" ");
    				var month = document.getElementById('nextMonth');
    				var monthNumber = month.innerHTML.split(" ");
    				var monthNumber1 = monthNumber[0];
    				console.log(monthNumber1);
    				var monthResult = 0;
    				for (var i = 0; i < allMonses.length; i++) {
    					if (monthNumber1===allMonses[i]) {
    						console.log(i);
    						monthResult = i;
    					}    				
    				}

    				var newObj = new createObjectEvent(inputes[0].value, inputes[1].value, inputes[2].value, year1Number[0], monthResult, getCurentBlock.firstChild.data)
    				getParent.style.display = "none";
    			}
    		}    		
    	}
    	flag=1;     
    });  

    var myObjects = [];
    function createObjectEvent(eventName, personName, description, year, month, day) {
    	this.eventName = eventName;
    	this.personName = personName;
    	this.description = description;
    	this.year = year;
    	this.month = month;
    	this.day = day;  
    	var mainArray = new Array(this.eventName, this.personName, this.description, this.year, this.month, this.day);
      myObjects.push(mainArray);
    }  
          var arrayMonth = myObjects;

          var arrayYear = myObjects; 

    var searchButton = document.getElementById('searchButton');
    
    //search
    searchButton.addEventListener('click', function() {
    	var searchValue = document.getElementById('searchValue');
    	if (myObjects=="") {
    		alert("добавьте собитие");
    	}
    	for (var i = 0; i < myObjects.length; i++) {

    		if (myObjects[i][0]===searchValue.value) {
    			var yearNewCreate = myObjects[i];
    			 arrayMonth = myObjects[i][4];
    			 arrayYear = myObjects[i][3];
    			var arrayDay = myObjects[i][5];
    			console.log(arrayDay);
    			i = myObjects.length;
    			var counterNew = i-1;
    			console.log("counterNew"+counterNew);

    			while (mainContent.firstChild) {
    				mainContent.removeChild(mainContent.firstChild);
    			}
    			var todayDate = new navigationYearMonth(allMonses[arrayMonth], arrayYear)
    			var nextMonth = document.getElementById('nextMonth');
    			control.removeChild(nextMonth); 
    			var nextYear = document.getElementById('nextYear');
    			control.removeChild(nextYear);  

    			var date1 = new Date(arrayYear, arrayMonth-1);
    			var dayNumber1 = date1.daysInMonth();
    			console.log(date1);

    			for (var i = 1; i < date1.getDay(); i++) {
    				var newDaySpan1 = document.createElement("div");
    				newDaySpan1.innerHTML = "_";
    				newDaySpan1.className = "someDay";
    				mainContent.appendChild(newDaySpan1); 
    			}

    			for (var i = 1; i <= dayNumber1; i++) {     	
    				var newDaySpan = document.createElement("div");
    				newDaySpan.innerHTML = i;
    				newDaySpan.className = "someDay";
    				mainContent.appendChild(newDaySpan); 
    				if (i==arrayDay) {
    					// console.log("counterNew"+counterNew);
    					// console.log(myObjects[counterNew]);
    					mainContent.removeChild(mainContent.lastChild);
    					var newDaySpan = document.createElement("div");
    					newDaySpan.innerHTML = i;
    					newDaySpan.className = "myday";
    					newDaySpan.style.backgroundColor="#d7eef3";
    					mainContent.appendChild(newDaySpan);
    					var wrapper = document.createElement("div");    		
    					wrapper.className = "myEvent";
    					newDaySpan.appendChild(wrapper);
    					var myForm = document.getElementById('myForm');
    					myForm2 = myForm.cloneNode(true);
    					myForm2.style.display = "block";
    					wrapper.appendChild(myForm2);

    					var insidValue = myForm2.getElementsByTagName('input');
    					insidValue[0].value = yearNewCreate[0];
    					insidValue[1].value = yearNewCreate[1];
    					insidValue[2].value = yearNewCreate[2];   

  		

    				}
            for (var j = 0; j < myObjects.length; j++) {
              if (myObjects[j][0]!==searchValue.value && myObjects[j][4]==arrayMonth && myObjects[j][3]==arrayYear) {
                for (var k = 0; k < mainContent.children.length; k++) {
                  if (mainContent.children[k].innerHTML==myObjects[j][5]) {
                    console.log(mainContent.children[k]);
                    console.log(myObjects[j]); 
                    mainContent.children[k].style.backgroundColor="#d7eef3";
                    var wrapper = document.createElement("div");       
                    wrapper.className = "myEvent";
                    newDaySpan.appendChild(wrapper);
                    var myForm = document.getElementById('myForm');
                    var myForm3 = myForm.cloneNode(true);
                    wrapper.appendChild(myForm3);
                    var insidValue = myForm3.getElementsByTagName('input');
                    insidValue[0].value = myObjects[j][0];
                    insidValue[1].value = myObjects[j][1];
                    insidValue[2].value = myObjects[j][2];  
                    myForm3.style.display = "none";
 

                  }

               }
             }
           }
    			}

    			thisDayNext = todayNext.getFullYear();
    			thisMonthNext = todayNext.getMonth()+1;
    			thisDayPrevious = todayPrevious.getFullYear();
    			thisMonthPrevious = todayPrevious.getMonth()+1;
    		}
    	}
      console.log(arrayMonth);
      console.log(arrayYear);
    });
   
